#FROM eclipse-temurin
#ARG JAR_FILE=target/*.jar
#COPY ${JAR_FILE} app.jar
#ENTRYPOINT ["java","-jar","/app.jar"]

# Build stage
FROM maven:3.6.0-jdk-8-slim AS build
WORKDIR /cgi
COPY . .
RUN mvn install

FROM openjdk:8
WORKDIR /cgi
COPY --from=build /cgi/target/dentistapp-1.0.jar .
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "dentistapp-1.0.jar"]